<x-layouts>
    {{-- <div class="container">
        <div class="row mt-5">
            <div class="col-md-12 mt-5">
              <form action="{{route('paragraph.update', compact('paragraph'))}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                  <label for="exampleFormControlInput1">Titolo</label>
                  <input name="title" value="{{$paragraph->title}}" type="text" class="form-control @error('title') is-invalid @enderror" id="exampleFormControlInput1" placeholder="Titolo">
                  @error('title')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{$message}}</strong>
                  </span>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="exampleFormControlTextarea1">Aggiungi Paragrafo</label>
                  <textarea name="body" class="form-control @error('body') is-invalid @enderror" id="summary-ckeditor" rows="3">{{$paragraph->body}}</textarea>
                  @error('body')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{$message}}</strong>
                  </span>
                  @enderror
                </div>
                @if($paragraph->draft == true)
                <p>Capitolo <strong>{{$paragraph->capital_id}}</strong> , se vuoi cambiare la posizione del capitolo all'interno del libro seleziona un nuovo capitolo</p>
                <div class="form-group">
                  <select name="capital_id" class="form-select" aria-label="Default select example">
                    <option selected value="{{$paragraph->capital_id}}">Seleziona un capitolo</option>
                    @foreach ($counts as $count)
                    <option value="{{$count->capital_id}}">Capitolo {{$count->capital_id}}</option>    
                    @endforeach
                  </select>
                </div>
                @endif
              <div class="form-check mt-5 mb-5">
                <input name="draft" class="form-check-input" type="checkbox" value="1" id="flexCheckDefault">
                <label class="form-check-label d-block" for="flexCheckDefault"> <h4>
                  Rendi il capitolo pubblico
                </h4>
              </label>
              <small>Se non viene selezionata questa opzione il capitolo verrà salvato come bozza</small>
              </div>
              
              <button class="btn btn-dark">Salva</button>
            </form>
        </div>
      </div>
    </div>
   --}}

<div class="row mt-5"></div>
       <div class="container shadow-lg">
                    <div class="row justify-content-center bg-white mt-5 pb-5">
                      <div class="col-10 mt-5">
                          <h1 class="titlenewbook">Modifica capitolo</h1>
                          <div class= "tilteline mb-4"></div>
                          <form action="{{route('paragraph.update', compact('paragraph'))}}" method="POST">
                            @csrf
                            @method('PUT')
                            {{-- title --}}
                                <div class="form-group mt-5">
                                  {{-- <label for="exampleFormControlInput1">Nome del libro</label> --}}
                                  <input name="title" value="{{$paragraph->title}}" type="text" class="form-control titleform @error('title') is-invalid @enderror" id="exampleFormControlInput1" placeholder="Titolo">
                                  @error('title')
                                  <span class="invalid-feedback" role="alert">    
                                    <strong>{{$message}}</strong>
                                  </span>
                                  @enderror
                                </div>
                                <div class="form-group">
                              
                                    <textarea name="body" class="form-control @error('body') is-invalid @enderror" id="summary-ckeditor" {{-- name="summary-ckeditor"--}}rows="3">{{$paragraph->body}}</textarea>
                                    @error('body')
                                    <span class="invalid-feedback" role="alert">
                                      <strong>{{$message}}</strong>
                                    </span>
                                    @enderror
                              </div>
                              @if($paragraph->draft == true)
                              <div class="form-group mt-5">
                                <h4 class="titlepublic mb-2"> Cambia ordine capitoli </h4> 
                                <select name="capital_id" class="form-select mb-1" aria-label="Default select example">  
                                  <option selected value="{{$paragraph->capital_id}}">Seleziona un capitolo</option>
                                  @foreach ($counts as $count)
                                  <option value="{{$count->capital_id}}">Capitolo {{$count->capital_id}}</option>    
                                  @endforeach
                                </select> <br>
                              <small class="titlepublicdescriptiion">Capitolo <strong>{{$paragraph->capital_id}}</strong> , se vuoi cambiare la posizione del capitolo all'interno del libro seleziona un nuovo capitolo</small>
                              </div>
                              @endif
                        <div class="form-check mt-5 mb-5">
                          <input name="draft" class="form-check-input" type="checkbox" value="1" id="flexCheckDefault">
                          <label class="form-check-label d-block" for="flexCheckDefault"> 
                            <h4 class="titlepublic">
                            Rendi il libro pubblico
                            </h4>    
                          </label>
                          <span class="titlepublicdescriptiion">Se non viene selezionata questa opzione il libro verrà salvato come bozza</span>
                        </div>

                      <button class=" btn btn-lg btn-block buttoncreate">Salva</button>
                      </form>
                  </div>
              </div>
          </div>
  






    <script src="//cdn.ckeditor.com/4.16.0/full/ckeditor.js"></script>
    <script>
    CKEDITOR.replace( 'summary-ckeditor', {
      filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
      filebrowserUploadMethod: 'form'
    });
    </script>
</x-layouts>
  