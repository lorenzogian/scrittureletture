<x-layouts>
      {{--    
            <div class="col-md-12 mt-5">
              <form action="{{route('paragraph.store')}}" method="POST">
                @csrf
                <div class="form-group">
                  <h1 class="mb-5">Aggiungi un capitolo</h1>
                </div>
                <div class="form-group">
                  <label for="exampleFormControlInput1">Titolo</label>
                  <input name="title" value="{{old('title')}}" type="text" class="form-control @error('title') is-invalid @enderror" id="exampleFormControlInput1" placeholder="Titolo">
                  @error('title')
                  <span class="invalid-feedback" role="alert">    
                    <strong>{{$message}}</strong>
                  </span>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="exampleFormControlTextarea1">Aggiungi Capitolo</label>
                  <textarea name="body" class="form-control @error('body') is-invalid @enderror" id="summary-ckeditor" rows="3">{{old('body')}}</textarea>
                  @error('body')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{$message}}</strong>
                  </span>
                  @enderror
                </div>
                <label class="mt-3"><h4> Assegna il libro per aggiungere un capitolo</h4> </label>
                <div class="form-group">
                    <select name="name" class="form-select" aria-label="Default select example">
                        @foreach ($paragraphs as $paragraph)
                        <option value="{{$paragraph->name}}" selected>{{$paragraph->name}}</option>    
                        @endforeach 
                    </select>
                </div>
                
                <div class="form-check mt-5 mb-5">
                  <input name="draft" class="form-check-input" type="checkbox" value="1" id="flexCheckDefault">
                  <label class="form-check-label d-block" for="flexCheckDefault"> <h4>
                    Rendi il capitolo pubblico
                  </h4>
                </label>
                <small>Se non viene selezionata questa opzione il capitolo verrà salvato come bozza</small>
                </div>
              <button class="btn btn-dark">Salva</button>
            </form>
        </div>
      </div>
    </div>
   --}}
  
    
    





<div class="row mt-5"></div>
  <div class="container shadow-lg">
      <div class="row justify-content-center bg-white mt-5 pb-5 ">
        <div class="col-10 mt-5">
                <h1 class="titlenewbook">Aggiungi un capitolo</h1>
                <div class= "tilteline mb-4"></div>
                <form action="{{route('paragraph.store')}}" method="POST">
                  @csrf
                  {{-- title --}}
                  <div class="form-group mt-5">
                    {{-- <label for="exampleFormControlInput1">Nome del libro</label> --}}
                    <input name="title" value="{{old('title')}}" type="text" class="form-control titleform @error('title') is-invalid @enderror" id="exampleFormControlInput1" placeholder="Titolo">
                    @error('title')
                    <span class="invalid-feedback" role="alert">    
                      <strong>{{$message}}</strong>
                    </span>
                    @enderror
                  </div>

                <div class="form-group">
                  <textarea name="body" class="form-control @error('body') is-invalid @enderror" id="summary-ckeditor" {{-- name="summary-ckeditor"--}}rows="3">{{old('body')}}</textarea>
                  @error('body')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{$message}}</strong>
                  </span>
                  @enderror
                </div>
                <label class="mt-3 "><h4 class="titlepublic"> Seleziona il libro per aggiungere un capitolo</h4> </label>
                <div class="form-group">
                    <select name="name" class="form-select" aria-label="Default select example">
                        @foreach ($paragraphs as $paragraph)
                        <option value="{{$paragraph->name}}" selected>{{$paragraph->name}}</option>    
                        @endforeach 
                    </select>
                </div>

                <div class="form-check mt-5 mb-5">
                  <input name="draft" class="form-check-input" type="checkbox" value="1" id="flexCheckDefault">
                  <label class="form-check-label d-block" for="flexCheckDefault"> 
                    <h4 class="titlepublic">
                    Rendi il libro pubblico
                    </h4>    
                  </label>
                  <span class="titlepublicdescriptiion">Se non viene selezionata questa opzione il libro verrà salvato come bozza</span>
              </div>

              <button class=" btn btn-lg btn-block buttoncreate">Salva</button>
            </form>
        </div>
     </div>
</div>

      <script src="//cdn.ckeditor.com/4.16.0/full/ckeditor.js"></script>
      <script>
      CKEDITOR.replace( 'summary-ckeditor', {
        filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
      });
      </script>
</x-layouts>