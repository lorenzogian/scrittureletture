<x-layouts>
    <div class="imagearticle">
        <div class="overlay"></div>
        <div class="container  h-100">
          <div class="d-flex h-100 text-center align-items-center">
            <div class="w-100 text-white">
              <h1 class="display-3">{{$article->title}}</h1>
              <p class="lead mb-5">With HTML5 Video and Bootstrap 4</p>
              <a href="#article" class="text-white mt-5 "> <svg xmlns="http://www.w3.org/2000/svg" width="45" height="45" fill="currentColor" class="bi bi-chevron-down icon" viewBox="0 0 16 16">
                <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/>
              </svg></a>
            </div>
          </div>
        </div>
    </div>
    <div class="conatiner">
        <div class="row">
            <img src="" alt="">
            <div class="col-12">
                {!!$article->body!!}
            </div>
        </div>
    </div>
</x-layouts>