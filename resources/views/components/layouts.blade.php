<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

   {{--  <title>{{ config('app.name', 'Laravel') }}</title> --}}
    
    <title>{{$title ?? 'Scritture & Letture' }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Scripts per CKEditor Script -->
    <script src="//cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Playfair+Display:ital,wght@0,400;0,500;0,600;0,700;0,800;0,900;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">


    <!-- Styles -->
    <link href="{{asset('css/app.css') }}" rel="stylesheet">
    <!-- favicon-->
    <link rel="shortcut icon"  href="{{Storage::url('/uploads/favicon_1619561086.png')}}"  type="image/x-icon">
</head>

@if(Request::path() != '/')

<x-_navbar/>
@endif
<body class="mb-5">
    <div id="app">
       {{--  <main class="py-4">
        </main> --}}
        {{$slot}}
    </div>
    @stack('scripts')
    
  {{--   <x-_footer/> --}}
</body>
</html>
