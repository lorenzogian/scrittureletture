<x-layouts>
    <div class="imagearticle mt-5">
        <div class="overlay"></div>
        <div class="container  h-100">
          <div class="d-flex h-100 text-center align-items-center">
            <div class="w-100 text-white">
              <h1 class="display-3">{{$article->title}}</h1>
            </div>
          </div>
        </div>
    </div>
    <div class="conatiner">
        <div class="row justify-content-center mt-5">
    
            <div class="col-9">
                {!!$article->body!!} 
        </div>
    </div>
</x-layouts>