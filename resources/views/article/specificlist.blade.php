<x-layouts>
<div class="container-fluid ">
  <div class="row mt-5 pb-5 justify-content-center bg-dark">
    <div class="col-10 mt-5 pb-5 text-center text-light">
      <svg class="mb-4" xmlns="http://www.w3.org/2000/svg" width="100" height="100" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
        <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
        <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
      </svg>
      <div>
        <span class="display-3 text-center edit">Modifica Libro</span> 
      </div>
    </div>
  </div>
</div>
        
    <div class="container bg-light">
        <div class="row justify-content-center mt-5 text-center">
            <div class="col-10 mt-5">
                <div>    
                  <h2 class="card-title">{{$article->title}}</h2>
                  @if($article->draft == 0)
                  <span class="label-category label-card text-danger ">Bozza</span>
                  @endif
                  <p  class="card-text undertitle"><small class="text-muted">Creato il {{$article->created_at}}</small></p>                 
                </div>
            </div>
        </div>
            <div class="row justify-content-center d-flex mt-5 pb-5">
                    <div class="col-12 mt-3 col-md-2 text-center">
                      <a href="{{ route('paragraph.create') }}" class="textsee">
                      <svg class="mb-3 textred" xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor" class="bi bi-journal-plus" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M8 5.5a.5.5 0 0 1 .5.5v1.5H10a.5.5 0 0 1 0 1H8.5V10a.5.5 0 0 1-1 0V8.5H6a.5.5 0 0 1 0-1h1.5V6a.5.5 0 0 1 .5-.5z"/>
                        <path d="M3 0h10a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-1h1v1a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H3a1 1 0 0 0-1 1v1H1V2a2 2 0 0 1 2-2z"/>
                        <path d="M1 5v-.5a.5.5 0 0 1 1 0V5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0V8h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0v.5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1z"/>
                      </svg> <br>
                      <span class="display-3 titlebutton"> <strong> Nuovo Capitolo</strong></span>
                      </a>
                    </div>
                    <div class="col-12 mt-3 col-md-2 text-center">
                      <a href="{{route('article.show', compact('article'))}}" class="textsee">
                      <svg class="mb-3" xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor" class="bi bi-journal-check" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M10.854 6.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 8.793l2.646-2.647a.5.5 0 0 1 .708 0z"/>
                        <path d="M3 0h10a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-1h1v1a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H3a1 1 0 0 0-1 1v1H1V2a2 2 0 0 1 2-2z"/>
                        <path d="M1 5v-.5a.5.5 0 0 1 1 0V5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0V8h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0v.5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1z"/>
                      </svg> <br>
                      <span class="display-3 titlebutton"> <strong> Vedi Libro</strong>
                      </span>
                    </a>
                    </div>
                    <div class="col-12 mt-3 col-md-2 text-center">
                      <a href="{{route('article.edit', compact('articlerev'))}}" class="textedit">
                      <svg class="mb-3" xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor" class="bi bi-journal-arrow-up" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M8 11a.5.5 0 0 0 .5-.5V6.707l1.146 1.147a.5.5 0 0 0 .708-.708l-2-2a.5.5 0 0 0-.708 0l-2 2a.5.5 0 1 0 .708.708L7.5 6.707V10.5a.5.5 0 0 0 .5.5z"/>
                        <path d="M3 0h10a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-1h1v1a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H3a1 1 0 0 0-1 1v1H1V2a2 2 0 0 1 2-2z"/>
                        <path d="M1 5v-.5a.5.5 0 0 1 1 0V5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0V8h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0v.5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1z"/>
                      </svg> <br>
                      <span class="display-3  titlebutton"> <strong> Modifica Intestazione Libro</strong>
                      </span>
                    </a>
                    </div>
                    <div class="col-12 mt-3 col-md-2 text-center">
                      <a href="" class="textdelete" type="button" data-toggle="modal" data-target="#elimina-{{$article->id}}">
          
                      <svg class="mb-3" xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor" class="bi bi-journal-x" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M6.146 6.146a.5.5 0 0 1 .708 0L8 7.293l1.146-1.147a.5.5 0 1 1 .708.708L8.707 8l1.147 1.146a.5.5 0 0 1-.708.708L8 8.707 6.854 9.854a.5.5 0 0 1-.708-.708L7.293 8 6.146 6.854a.5.5 0 0 1 0-.708z"/>
                        <path d="M3 0h10a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-1h1v1a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H3a1 1 0 0 0-1 1v1H1V2a2 2 0 0 1 2-2z"/>
                        <path d="M1 5v-.5a.5.5 0 0 1 1 0V5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0V8h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0v.5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1z"/>
                      </svg> <br>
                      <span class="display-3  titlebutton"> <strong> Cancella Libro</strong>
                      </span>
                    </a>
                    </div>
                    @if($article->progress == false)
                    <div class="col-12 mt-3 col-md-2 text-center">
                      <form action="{{route('article.finished', compact('articlerev'))}}"  method="POST">
                        @method('PUT')
                        @csrf
                        <button class="button2">
                        <svg class="mb-3" xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor" class="bi bi-check-square" viewBox="0 0 16 16">
                          <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                          <path d="M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.235.235 0 0 1 .02-.022z"/>
                        </svg><br>
                      <span class="display-3  titlebutton"> <strong> Libro terminato</strong>
                      </span>
                      </button>
                    </form>
                    </div>
                    @else
                    <div class="col-12 mt-3 col-md-2 text-center">
                      <form action="{{route('article.finished', compact('articlerev'))}}"  method="POST">
                        @method('PUT')
                        @csrf
                        <button class="button2">
                          <svg class="mb-3"xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor" class="bi bi-file-x" viewBox="0 0 16 16">
                            <path d="M6.146 6.146a.5.5 0 0 1 .708 0L8 7.293l1.146-1.147a.5.5 0 1 1 .708.708L8.707 8l1.147 1.146a.5.5 0 0 1-.708.708L8 8.707 6.854 9.854a.5.5 0 0 1-.708-.708L7.293 8 6.146 6.854a.5.5 0 0 1 0-.708z"/>
                            <path d="M4 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H4zm0 1h8a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1z"/>
                          </svg><br>
                      <span class="display-3  titlebutton"> <strong> Libro in scrittura</strong>
                      </span>
                      </button>
                    </form>
                    </div>
                    @endif

                    <div class="modal fade" id="elimina-{{$article->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body text-center">
                          <span> <strong class="text-danger"> Sei sicuro di voler eliminare questo libro? </strong>  </span>
                          <div>
                            <small> Verranno cancellati anche tutti i capitoli annessi al libro. <br> Una volta cancellato non potrai più recuperare il libro.</small>
                          </div>
                          </div>
                          <div class="modal-footer justify-content-center">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
                            <form action="{{route('article.delete', compact('article'))}}" method="POST">
                              @method('DELETE')
                              @csrf
                              <button class="btn btn-danger" type="submit"> Elimina</button>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
            </div>  
    </div>

 

    <div class="container">
      <div class="row justify-content-center mt-5 text-center">  
        <div class="table-responsive">           
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th scope="col">N. Capitolo</th>
                      <th scope="col">Titolo</th>
                      <th scope="col">Vedi Capitolo</th>
                      <th scope="col">Modifica Capitolo</th>
                      <th scope="col">Elimina Capitolo</th>
                      <th scope="col">Cambia posizione capitolo</th>
                      <th scope="col">Converti in Bozza</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($paragraphs as $paragraph)
                    <tr>
                      <th scope="row">
                        @if($paragraph->draft == 0)
                        <span class="label-category label-card text-danger ">Bozza</span>
                        @else
                        {{$paragraph->capital_id}}
                        @endif
                      </th>
                      <td>{{$paragraph->title}}</td>
                      <td> <a href="{{route('article.specificshowparagraph', compact('paragraph'))}}" class="textsee">
                        <svg class="mb-3" xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-journal-check" viewBox="0 0 16 16">
                          <path fill-rule="evenodd" d="M10.854 6.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 8.793l2.646-2.647a.5.5 0 0 1 .708 0z"/>
                          <path d="M3 0h10a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-1h1v1a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H3a1 1 0 0 0-1 1v1H1V2a2 2 0 0 1 2-2z"/>
                          <path d="M1 5v-.5a.5.5 0 0 1 1 0V5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0V8h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0v.5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1z"/>
                        </svg>
                      </a>
                        
                      <td> 
                        <a href="{{route('paragraph.edit', compact('paragraph'))}}" class="textedit">
                          <svg class="mb-3" xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-journal-arrow-up" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M8 11a.5.5 0 0 0 .5-.5V6.707l1.146 1.147a.5.5 0 0 0 .708-.708l-2-2a.5.5 0 0 0-.708 0l-2 2a.5.5 0 1 0 .708.708L7.5 6.707V10.5a.5.5 0 0 0 .5.5z"/>
                            <path d="M3 0h10a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-1h1v1a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H3a1 1 0 0 0-1 1v1H1V2a2 2 0 0 1 2-2z"/>
                            <path d="M1 5v-.5a.5.5 0 0 1 1 0V5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0V8h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0v.5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1z"/>
                          </svg>
                        </a>
                      <td>
                        <a  class="text-danger" type="button" data-toggle="modal" data-target="#elimina-{{$paragraph->id}}">
          
                          <svg class="mb-3" xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-journal-x" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M6.146 6.146a.5.5 0 0 1 .708 0L8 7.293l1.146-1.147a.5.5 0 1 1 .708.708L8.707 8l1.147 1.146a.5.5 0 0 1-.708.708L8 8.707 6.854 9.854a.5.5 0 0 1-.708-.708L7.293 8 6.146 6.854a.5.5 0 0 1 0-.708z"/>
                            <path d="M3 0h10a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-1h1v1a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H3a1 1 0 0 0-1 1v1H1V2a2 2 0 0 1 2-2z"/>
                            <path d="M1 5v-.5a.5.5 0 0 1 1 0V5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0V8h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0v.5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1z"/>
                          </svg> 
                        </a>
                      </td>
                      <td>
                        @if($paragraph->draft != 0)
                        <form action="{{route('paragraph.change', compact('paragraph'))}}" method="POST">
                          @csrf
                          @method('PUT')
                          <div class="form-group">
                            <select name="capital_id" class="form-select" aria-label="Default select example">
                                <option selected value="{{$paragraph->capital_id}}">Seleziona un capitolo</option>
                                @foreach ($counts as $count)
                                <option value="{{$count->capital_id}}">Capitolo {{$count->capital_id}}</option>    
                                @endforeach
                            </select>
                            <button class="btn btn-sm mt-3 mt-md-0 btn-secondary">Salva</button>
                          </div>
                        </form>
                        @endif
                      </td>
                      <td>  
                        @if($paragraph->draft != 0)
                        <form action="{{route('paragraph.draft', compact('paragraph'))}}" method="POST">
                          @method('PUT')
                          @csrf
                          <button class="btn btn-sm btn-secondary" type="submit"> Converti</button>
                        </form>
                        @endif
                    </td>
                    </tr>
                    <div class="modal fade" id="elimina-{{$paragraph->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body text-center">
                          <span> <strong class="text-danger"> Sei sicuro di voler eliminare questo capitolo? </strong>  </span>
                          <div>
                            <small> Una volta cancellato non potrai più recuperarlo.</small>
                          </div>
                          </div>
                          <div class="modal-footer justify-content-center">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
                            <form action="{{route('paragraph.delete', compact('paragraph'))}}" method="POST">
                              @method('DELETE')
                              @csrf
                              <button class="btn btn-danger" type="submit"> Elimina</button>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                    @endforeach 
                   
                  </tbody>
                </table>
              </div> 
        </div>
    </div>
  
</x-layouts>