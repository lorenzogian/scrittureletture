<x-layouts>
    <style type="text/css" media="print" >
      * { display: none; }
  </style>
  <div class="headerarticle" style="background-image: linear-gradient(to bottom, rgba(56, 56, 56, 0.7), rgba(56, 56, 56, 0.7)), url({{Storage::url($articlerev->img)}})">
    <div class="overlay"></div>
      <div class="container h-100" >
        <div class="d-flex h-100 text-center align-items-center" >
          <div class="w-100 text-white">
            <h1 class="display-3 p-2">{{$articlerev->title}} </h1>
          </div>
        </div>
      </div>
    </div>

    <div class="conatiner"  ondragstart="return false;" onselectstart="return false;"  oncontextmenu="return false;" onload="clearData();" onblur="clearData();" onmousedown="BloccaMouse()">
        <div class="row justify-content-center mt-5">
            <div class="col-9">
              <div class="mb-5 h-40">
                <h2>Indice</h2>
                @foreach ($paragraphs as $paragraph)
                <div class="mt-3">
                  <a href="#{{$paragraph->id}}" class="index">{{$paragraph->capital_id}} - {{$paragraph->title}}</a>  <br>
                </div>
                @endforeach
              </div>
                {!!$articlerev->body!!} 
                @foreach ($paragraphs as $paragraph)            
                <div id="{{$paragraph->id}}"></div>
                <div class="mt-5 mb-5 border-bottom border-secondary"></div>
                <h2 class="mt-5 mb-4">{{$paragraph->capital_id}} - {{$paragraph->title}}</h2>
                <div class="mt-5">
                 {!!$paragraph->body!!}
               </div>
                
                @endforeach
            </div>
            
        </div>
    </div>
  
</x-layouts>