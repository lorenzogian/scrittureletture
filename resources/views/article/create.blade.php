{{-- <x-layouts>
  <div class="container">
      <div class="row mt-5">
          <div class="col-md-12 mt-5">
            <form action="{{route('article.store')}}" method="POST">
              @csrf
              <div class="form-group">
                <label for="exampleFormControlInput1">Titolo</label>
                <input name="title" value="{{old('title')}}" type="text" class="form-control @error('title') is-invalid @enderror" id="exampleFormControlInput1" placeholder="Titolo">
                @error('title')
                <span class="invalid-feedback" role="alert">    
                  <strong>{{$message}}</strong>
                </span>
                @enderror
              </div>
              <div class="form-group"> 
                <label for="exampleFormControlTextarea1">Sinossi</label>
                <textarea name="synopsis" class="form-control @error('synopsis') is-invalid @enderror" id="synopsis" cols="1" rows="2" placeholder="Sinossi"> {{old('body')}}</textarea>
                <small>Breve riassunto di max:200 caratteri</small>
                @error('synopsis')
                <span class="invalid-feedback" role="alert">
                  <strong>{{$message}}</strong>
                </span>
                @enderror
              </div>
              <div class="form-group">
                <label for="exampleFormControlTextarea1">Articolo</label>
                <textarea name="body" class="form-control @error('body') is-invalid @enderror" id="summary-ckeditor"rows="3">{{old('body')}}</textarea>
                @error('body')
                <span class="invalid-feedback" role="alert">
                  <strong>{{$message}}</strong>
                </span>
                @enderror
              </div>
              <div class="form-check mt-5 mb-5">
                <input name="draft" class="form-check-input" type="checkbox" value="1" id="flexCheckDefault">
                <label class="form-check-label d-block" for="flexCheckDefault"> <h4>
                  Rendi l'articolo pubblico
                </h4>
              </label>
              <small>Se non viene selezionata questa opzione l'articolo verrà salvato come bozza</small>
            </div>
            <button class="btn btn-dark">Salva</button>
          </form>
      </div>
    </div>
  </div> --}}





<x-layouts>
 {{--  <div class="container">
      <div class="row mt-5">
          <div class="col-md-12 mt-5">
            <form action="{{route('article.store')}}" method="POST">
              @csrf
              <div class="form-group">
                <label for="exampleFormControlInput1">Titolo</label>
                <input name="title" value="{{old('title')}}" type="text" class="form-control @error('title') is-invalid @enderror" id="exampleFormControlInput1" placeholder="Titolo">
                @error('title')
                <span class="invalid-feedback" role="alert">    
                  <strong>{{$message}}</strong>
                </span>
                @enderror
              </div>
              <div class="form-group"> 
                <label for="exampleFormControlTextarea1">Sinossi</label>
                <textarea name="synopsis" class="form-control @error('synopsis') is-invalid @enderror" id="synopsis" cols="1" rows="2" placeholder="Sinossi"> {{old('body')}}</textarea>
                <small>Breve riassunto di max:200 caratteri</small>
                @error('synopsis')
                <span class="invalid-feedback" role="alert">
                  <strong>{{$message}}</strong>
                </span>
                @enderror
              </div> --}}
             {{--  <div class="form-group">
                <label for="exampleFormControlTextarea1">Articolo</label>
                <textarea name="body" class="form-control @error('body') is-invalid @enderror" id="summary-ckeditor" rows="3">{{old('body')}}</textarea>
                @error('body')
                <span class="invalid-feedback" role="alert">
                  <strong>{{$message}}</strong>
                </span>
                @enderror
              </div> --}}
       {{--        <div class="form-check mt-5 mb-5">
                <input name="draft" class="form-check-input" type="checkbox" value="1" id="flexCheckDefault">
                <label class="form-check-label d-block" for="flexCheckDefault"> <h4>
                  Rendi l'articolo pubblico
                </h4>
              </label>
              <small>Se non viene selezionata questa opzione l'articolo verrà salvato come bozza</small>
             </div>
            <button class="btn btn-dark">Salva</button>
          </form>
      </div>
    </div>
  </div>

 --}}
  

<div class="row mt-5"></div>
<div class="container shadow-lg">
  <div class="row justify-content-center bg-white mt-5 pb-5">
    <div class="col-12 col-md-4 mt-5 pl-5 line">
        <h1 class="titlenewbook">Scrivi Libro </h1>
        <div class= "tilteline mb-4"></div>
        <form action="{{route('article.store')}}" method="POST" enctype="multipart/form-data">
          @csrf
          {{-- title --}}
          <div class="form-group mt-5">
            {{-- <label for="exampleFormControlInput1">Nome del libro</label> --}}
            <input name="title" value="{{old('title')}}" type="text" class="form-control titleform @error('title') is-invalid @enderror" id="exampleFormControlInput1" placeholder="Titolo">
            @error('title')
            <span class="invalid-feedback" role="alert">    
              <strong>{{$message}}</strong>
            </span>
            @enderror
          </div>

          <div class="form-group mt-5"> 
            {{-- <label for="exampleFormControlTextarea1">Sinossi</label> --}}
            <textarea name="synopsis" placeholder="Sinossi" class="form-control titleform @error('synopsis') is-invalid @enderror" id="synopsis" cols="1" rows="2">{{old('body')}}</textarea>
            <span class="titlepublicdescriptiion pl-3">Breve riassunto di max:200 caratteri</span>
            @error('synopsis')
            <span class="invalid-feedback" role="alert">
              <strong>{{$message}}</strong>
            </span>
            @enderror
          </div>
        
          
          <div class="form-check mt-5 mb-5">
            <input name="draft" class="form-check-input" type="checkbox" value="1" id="flexCheckDefault">
            <label class="form-check-label d-block" for="flexCheckDefault"> <h4 class="titlepublic">
              Rendi il libro pubblico
            </h4>
            
          </label>
          <span class="titlepublicdescriptiion">Se non viene selezionata questa opzione il libro verrà salvato come bozza</span>
         </div>
    </div>
    <div class="col-12 col-md-8 mt-5">

      <div class="form-group">
       {{--  <label for="exampleFormControlTextarea1">Libro</label> --}}
        <textarea name="body" class="form-control @error('body') is-invalid @enderror" id="summary-ckeditor" {{-- name="summary-ckeditor"--}}rows="3">{{old('body')}}</textarea>
        @error('body')
        <span class="invalid-feedback" role="alert">
          <strong>{{$message}}</strong>
        </span>
        @enderror
      </div>
      <div class="input-group mt-5">
        <div class="custom-file">
          <input  class="form-control titleform @error('img') is-invalid @enderror" type="file" id="img" name="img" aria-describedat="img">
        </div>
        @error('img')
        <span class="invalid-feedback" role="alert">
          <strong>{{$message}}</strong>
        </span>
        @enderror
      </div>

      <button class=" mt-5 btn btn-lg btn-block buttoncreate">Salva</button>
    </form>
    </div>
  </div>
</div>

{{--  <script src="//cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script> --}}
<script>
      CKEDITOR.replace( 'summary-ckeditor', {
        filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
      });
 </script>

</x-layouts>