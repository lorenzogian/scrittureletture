<x-layouts>
  {{--   <div class="container">
        <div class="row mt-5">
            <div class="col-md-12 mt-5">
              <form action="{{route('article.upload', compact('articlerev')) }}" method="POST">
                @method('PUT')
                @csrf
                <div class="form-group">
                  <label for="exampleFormControlInput1">Titolo</label>
                  <input name="title" value="{{$articlerev->title}}" type="text" class="form-control @error('title') is-invalid @enderror" id="exampleFormControlInput1" placeholder="Titolo">
                  @error('title')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{$message}}</strong>
                  </span>
                  @enderror
                </div>
                <div class="form-group"> 
                  <label for="exampleFormControlTextarea1">Sinossi</label>
                  <textarea name="synopsis" class="form-control titleform @error('synopsis') is-invalid @enderror" id="synopsis" cols="1" rows="2" placeholder="Sinossi"> {{old('synopsis')}} {{$articlerev->synopsis}}</textarea>
                  <small>Breve riassunto di max:200 caratteri</small>
                  @error('synopsis')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{$message}}</strong>
                  </span>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="exampleFormControlTextarea1">Libro</label>
                  <textarea name="body" placeholder="Sinossi" class="form-control titleform @error('synopsis') is-invalid @enderror" id="synopsis" cols="1" rows="2">{{old('body')}} {{$articlerev->body}}</textarea>
                  @error('body')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{$message}}</strong>
                  </span>
                  @enderror
                </div>
                <div class="form-check mt-5 mb-5">
                  <input name="draft" class="form-check-input" type="checkbox" value="1" id="flexCheckDefault">
                  <label class="form-check-label d-block" for="flexCheckDefault"> <h4>
                    Rendi il libro pubblico
                  </h4>
                </label>
                <small>Se non viene selezionata questa opzione il libro verrà salvato come bozza</small>
              </div>
              <button class="btn btn-dark" type="submit" >Modifica</button>
            </form>
        </div>
      </div>
    </div>
   --}}
  
    
   














  <div class="container">
      <div class="row mt-5">

<div class="row mt-5"></div>
<div class="container shadow-lg">
<div class="row justify-content-center bg-white mt-5">
  <div class="col-10 mt-5">
      <h1 class="titlenewbook">Modifica intestazione sito</h1>
      <div class= "tilteline mb-4"></div>
      <form action="{{route('article.upload', compact('articlerev')) }}" method="POST">
        @method('PUT')
        @csrf
        {{-- title --}}
        <div class="form-group mt-5">
          {{-- <label for="exampleFormControlInput1">Nome del libro</label> --}}
          <input name="title" value="{{$articlerev->title}}" type="text" class="form-control titleform @error('title') is-invalid @enderror" id="exampleFormControlInput1" placeholder="Titolo">
          @error('title')
          <span class="invalid-feedback" role="alert">    
            <strong>{{$message}}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group"> 
       {{--    <label for="exampleFormControlTextarea1">Sinossi</label> --}}
          <textarea name="synopsis" class="form-control @error('synopsis') is-invalid @enderror" id="synopsis" cols="1" rows="2" placeholder="Sinossi"> {{old('synopsis')}} {{$articlerev->synopsis}}</textarea>
          <small>Breve riassunto di max:200 caratteri</small>
          @error('synopsis')
          <span class="invalid-feedback" role="alert">
            <strong>{{$message}}</strong>
          </span>
          @enderror
        </div>

      <div class="form-group">
        <textarea name="body" class="form-control @error('body') is-invalid @enderror" id="summary-ckeditor" {{-- name="summary-ckeditor"--}}rows="3">{{old('body')}}</textarea>
        @error('body')
        <span class="invalid-feedback" role="alert">
          <strong>{{$message}}</strong>
        </span>
        @enderror
      </div>
 

    <div class="form-check mt-5 mb-5">
      <input name="draft" class="form-check-input" type="checkbox" value="1" id="flexCheckDefault">
      <label class="form-check-label d-block" for="flexCheckDefault"> 
        <h4 class="titlepublic">
        Rendi il libro pubblico
        </h4>    
      </label>
      <span class="titlepublicdescriptiion">Se non viene selezionata questa opzione il libro verrà salvato come bozza</span>
   </div>

    <button class=" btn btn-lg btn-block buttoncreate">Salva</button>
  </form>
  </div>
</div>
</div>

    <script src="//cdn.ckeditor.com/4.16.0/full/ckeditor.js"></script>
    <script>
    CKEDITOR.replace( 'summary-ckeditor', {
      filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
      filebrowserUploadMethod: 'form'
    });
    </script>
</x-layouts>
