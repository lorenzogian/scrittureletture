<x-layouts>
  <style type="text/css" media="print" >
    * { display: none; }
</style>
                <header>
                  <div class="overlay"></div>
                    <div class="container  h-100">
                      <div class="d-flex h-100 text-center align-items-center">
                        <div class="w-100 text-white">
                          <div class=" navbarr"> 
                            @if(Request::path() == '/')
                            <nav id="scroll" class="navbar navbar-expand-md navbar-dark  fixed-top">
                              <div class="container">
                                  <a class="navbar-brand" href="{{ url('/') }}">
                                     <img src="/storage/uploads/scritture&letture150bianco_1619561065.png" alt="">
                                  </a>
                                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                                      <span class="navbar-toggler-icon"></span>
                                  </button>
                          
                                  <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                      <!-- Left Side Of Navbar -->
                                      <ul class="navbar-nav ml-auto">
                                          @auth
                                          {{-- <li class="nav-item">
                                              <a class="nav-link" href="{{ route('paragraph.create') }}">Crea Paragrafo</a>
                                          </li> --}}
                                          <li class="nav-item">
                                              <a class="nav-link" href="{{ route('article.create') }}">Crea Libro</a>
                                          </li>
                                          <li class="nav-item">
                                              <a class="nav-link" href="{{ route('list') }}">Lista Libri</a>
                                          </li>
                                              
                                          @endauth
                          
                                      </ul>
                          
                                      <!-- Right Side Of Navbar -->
                                      <ul class="navbar-nav ">
                                          <!-- Authentication Links -->
                                          @guest
                                              @if (Route::has('login'))
                                                  <li class="nav-item">
                                                      <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                                  </li>
                                              @endif
                                              
                                          {{--     @if (Route::has('register'))
                                                  <li class="nav-item">
                                                      <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                                  </li>
                                              @endif --}}
                                          @else
                                              <li class="nav-item dropdown">
                                                  <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                                      {{ Auth::user()->name }}
                                                  </a>
                          
                                                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                      <a class="dropdown-item" href="{{ route('logout') }}"
                                                         onclick="event.preventDefault();
                                                                       document.getElementById('logout-form').submit();">
                                                          {{ __('Logout') }}
                                                      </a>
                          
                                                      <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                                          @csrf
                                                      </form>
                                                  </div>
                                              </li>
                                          @endguest
                                      </ul>
                                  </div>
                              </div>
                            </nav>
                            @endif   
                        </div>
                          <h1 class="display-3 titleresponsive">Scritture <span class="colortitle">&</span>  Letture</h1>
                          <h1 class="display-3 titlemobile">Scritture  <br> <span class="colortitle">&</span> <br>  Letture</h1>
                          <p  class="undertitle lead mt-4 mt-lg-2 mb-5">QUI VA INSERITO IL MOTTO DEL SITO</p>
                          <a href="#article" class="text-white mt-5 "> <svg xmlns="http://www.w3.org/2000/svg" width="45" height="45" fill="currentColor" class="bi bi-chevron-down icon" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/>
                          </svg></a>
                        </div>
                      </div>
                    </div>
                  </header>
                  
                  <div id="article"></div>
                  <div class="container">
                    <div class="row mt-5 mb-5 pt-5 pb-5">
                      <div class="col-12">
                        <h2 class="text-center section-title mt-5"><span>I miei Libri</span></h2>
                      </div>
                    </div>
                  </div>
                  {{-- @foreach ($articles as $article)
                    <section class="my-5">
                      <div class="container">
                        <div class="row justify-content-center bg-white align-items-center">
                          <div class="col-md-3 ">
                            @if($article->img == false)
                            <img class="imagearticlesx img-fluid py-4" src="{{Storage::url($article->img)}}" alt="">
                            @else
                            <img class="imagearticlesx img-fluid py-4" src="https://picsum.photos/200/300" alt="">
                            @endif
                          </div>
                        <div class="col-md-8"> <h2 class="titlearticle">{{$article->title}}</h2>
                            
                              <div class="mt-2"> <p> Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quisquam exercitationem esse perferendis unde delectus fugit voluptatem rerum quidem ducimus, dolorum, odio voluptatibus corrupti deleniti, veniam dicta id blanditiis reprehenderit iste?r</p></div>
                              <a class="button alink" type="submit" role="button" href="{{route('article.show', compact('article'))}}" > vedi libro</a>
                            </div>
                        </div>
                     </div>
                   </section>
                   @endforeach --}}
              
             
  <div class="container"  ondragstart="return false;" onselectstart="return false;"  oncontextmenu="return false;" onload="clearData();" onblur="clearData();" onmousedown="BloccaMouse()">
        <div class="row mb-5 justify-content-center">
          @foreach ($articles as $article)  
                <div class="col-12 col-sm-12 col-md-10 col-lg-6 mb-5">
                      <div class="  ricettaphoto">
                          <div class="{{-- imageblock backgroundcolor --}}"> 
  
                            <img class="imageblock  {{-- img-fluid --}}"  src="{{Storage::url($article->img)}}" alt="">
                            </div>
                            <div class="card titoloregione">
                              <div class="card-header bg-white border-bottom-0"><h1>{{$article->title}}</h1> </div>
                              <div class="card-body"><p class="pcard pb-auto">{!!Str::limit($article->synopsis, 150)!!} </p></div>
                              <div class="card-footer bg-white border-top-0"> <a class="button alink" type="submit" role="button" href="{{route('article.show', compact('article'))}}" > vedi libro</a></div>
                            </div>
                            {{-- <div class="card"></div>
                            <div class="card-body titoloregione"> <h1>{{$article->title}}</h1> <p class="pcard pb-auto">{!!Str::limit($article->synopsis, 90)!!} </p> <br> 
                            <div class= "card-footer mt-auto" >
                              <a class="button alink" type="submit" role="button" href="{{route('article.show', compact('article'))}}" > vedi libro</a></div>
                            </div> --}}
                      </div>
                </div>
                @endforeach
        </div>
  </div>

  <div class="container">
    <div class="row mt-5 mb-5 pt-5 pb-5">
      <div class="col-12">
        <h2 class="text-center section-title mt-5"><span>I libri in costruzione</span></h2>
      </div>
    </div>
  </div>

<div class="container-fluid" ondragstart="return false;" onselectstart="return false;"  oncontextmenu="return false;" onload="clearData();" onblur="clearData();" onmousedown="BloccaMouse()">
    <div class="row mb-5 justify-content-center">
            @foreach ($articlesprogress as $articlesprogres)  
              <div class="col-12 col-sm-12 col-md-12 col-lg-12 mb-5 mr-0 ml-0 p-0">
                    <div class="ricettaphoto">                     
                          <div class="headerarticle imageblock" style="background-image: linear-gradient(to bottom, rgb(0 0 0), rgba(49, 49, 49, 0.356)), url({{Storage::url($articlesprogres->img)}})">
                          </div>
                        <div class="card bg-transparent border-0 titlenotfinished text-white">
                            <div class="card-header border-bottom-0"><h1>{{$articlesprogres->title}}</h1> </div>
                            <div class="card-body"><p class="pcard pb-auto">{!!Str::limit($articlesprogres->synopsis, 150)!!} </p></div>
                            <div class="card-footer  border-top-0"> <a class="button alink" type="submit" role="button" href="{{route('articlenofinish.show', compact('articlesprogres'))}}" > vedi libro</a></div>
                        </div>    
                    </div>
               </div>
            @endforeach
    </div>
</div>

   
</x-layouts>
