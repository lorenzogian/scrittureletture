<x-layouts>
    <div id="article"></div>
    <div class="container">
      <div class="row mt-5 mb-2 pt-5 pb-3">
        <div class="col-12">
          <h2 class="text-center section-title mt-5"><span>Lista Libri</span></h2>
        </div>
      </div>
    </div>
    <div class="container">
        <div class="row justify-content-center mt-5">
            <div class="col-8 mt-5">
                @foreach ($a  as $article)
                <div class="card  mb-3" {{-- style="max-width: 100%px;" --}}>
                         <div class="row g-0 ml-0 mr-0 pr-0 pl-0">
                            <div class="imagelist col-md-3 col-3 imagearticlesx img-fluid" style="background-image: url({{Storage::url($article->img)}}">     
                            </div>      
                            <div class="col-12 col-md-8 mt-auto mb-auto ml-0 mr-0">
                              <div class="imagelistmobile col-12 imagearticlesx img-fluid" style="background-image: url({{Storage::url($article->img)}}">     
                              </div> 
                                    <div class="card-body">
                                        <h2 class="card-title">{{$article->title}}</h2>
                                        @if($article->draft == 0)
                                        <span class="label-category label-card text-danger ">Bozza</span>
                                        @endif
                                        <p class="card-text mt-2 pcardlist">{!! Str::limit($article->synopsis, 200)!!}</p>
                                        <p class="card-text pcardlist"><small class="text-muted">Creato il {{$article->created_at}}</small></p>
                                        <a class="btn btn-sm btn-dark" type="submit" role="button" href="{{route('article.specificlist', compact('article'))}}" > Vedi dettagli libro</a>
                                    </div>
                            </div>
                        </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</x-layouts>