<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\CKEditorController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ParagraphController;
use App\Models\Article;
use App\Models\Paragraph;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
return view('home');
});

/* Route::get('/uplaod', [Article::class, 'index'])->name('article.upload'); */
Route::get('/crea-libro', [ArticleController::class, 'create'])->name('article.create');
Route::post('/ckeditor/image_upload', [CKEditorController::class, 'upload'])->name('upload');
Route::post('/crea-libro/store', [ArticleController::class, 'store'])->name('article.store');

Route::get('/show/{article}', [HomeController::class, 'show'])->name('article.show');
Route::get('/show/{articlesprogres}', [HomeController::class, 'show'])->name('articlenofinish.show');
Route::get('/modifica-libro/{articlerev}', [ArticleController::class, 'edit'])->name('article.edit');
Route::put('/uplaod/{articlerev}', [ArticleController::class, 'update'])->name('article.upload');
Route::delete('/delete/{article}', [ArticleController::class, 'destroy'])->name('article.delete');

Route::put('/finished/{articlerev}', [ArticleController::class, 'finished'])->name('article.finished');



Route::get('/lista-articoli', [ArticleController::class, 'list'])->name('list');

Route::get('/crea-paragrafo',[ParagraphController::class, 'create'])->name('paragraph.create');
Route::post('/crea-paragrafo/store',[ParagraphController::class, 'store'])->name('paragraph.store');
Route::get('/modifica-libro-list/{article}', [ArticleController::class, 'special'])->name('article.specificlist');
Route::get('/show-libro-list/{article}', [ArticleController::class, 'specificshow'])->name('article.specificshow');
Route::get('/show-paragrafo-list/{paragraph}', [ParagraphController::class, 'specificshowparagraph'])->name('article.specificshowparagraph');
Route::get('/modifica-paragrafo/{paragraph}',[ParagraphController::class, 'edit'])->name('paragraph.edit');
Route::put('/modifica-paragrafo/update/{paragraph}',[ParagraphController::class, 'update'])->name('paragraph.update');

Route::put('/converti-in-bozza/{paragraph}',[ParagraphController::class, 'draft'])->name('paragraph.draft');

Route::put('/modifica-paragrafo/change/{paragraph}',[ParagraphController::class, 'change'])->name('paragraph.change');

Route::delete('/delete-paragraph/{paragraph}', [ParagraphController::class, 'destroy'])->name('paragraph.delete');

Route::get('/pannello-di-controllo', [AdminController::class, 'index'])->name('roles.admin');


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
