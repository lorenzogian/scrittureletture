<?php

namespace App\Http\Controllers;

use App\Http\Requests\EditParagraphRequest;
use App\Http\Requests\ParagraphRequest;
use App\Models\Article;
use App\Models\NameParagaph;
use App\Models\Paragraph;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ParagraphController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(){
     $paragraphs = NameParagaph::all();
     return view('paragraph.create', compact('paragraphs'));
    }

    public function store(ParagraphRequest $request){
        if($request->draft == true){ 
        $count=Paragraph::all()->where('name', $request->name)->where('capital_id','!=' , 0 )->count();;
        $addpargraph = new Paragraph();
        $addpargraph->capital_id = $count+1;
        $addpargraph->title = $request->title;
        $addpargraph->draft = true;
        $addpargraph->name = $request->name;
        $addpargraph->body = $request->body;
        $addpargraph->save();
        } else{
        $addpargraph = new Paragraph();    
        $addpargraph->capital_id = 0;
        $addpargraph->title = $request->title;
        $addpargraph->draft = false;
        $addpargraph->name = $request->name;
        $addpargraph->body = $request->body;
        $addpargraph->save();
        }

        $addpargraph->title = $request->title;
        $addpargraph->draft = $request->draft == true ? $request->draft : false ;
        $addpargraph->name = $request->name;
        $addpargraph->body = $request->body;
        $addpargraph->save();
     
        $article = Article::all()->where('title', $addpargraph->name)->first()->id; 
        return redirect(route('article.specificlist', compact('article')));

    }
    public function edit(Paragraph $paragraph){
     
        $counts=Paragraph::all()->where('name', $paragraph->name)->where('capital_id','!=' , 0 )->sortBy('capital_id');
        return view('paragraph.edit', compact('paragraph','counts'));
    }

    public function specificshowparagraph(Paragraph $paragraph){
        return view('article.specificshowparagraph', compact('paragraph'));
     }

     public function update(Paragraph $paragraph, EditParagraphRequest $request){
   
            if($paragraph->capital_id > $request->capital_id && $paragraph->capital_id !== 0 && $request->draft == true){
                $paragraph->capital_id = 0;
                $paragraph->save();
                $counts=Paragraph::all()->where('name', $paragraph->name)->where('capital_id', '>=', $request->capital_id);
                $c = 0;
                foreach($counts as $count){
                    $count->capital_id = $request->capital_id + 1 + $c;
                    $count->save();
                    $c +=1;
                    $paragraph->capital_id = $request->capital_id;
                    $paragraph->save();
                }
            } 
            elseif($paragraph->capital_id < $request->capital_id && $paragraph->capital_id !== 0 && $request->draft == true){
                $counts=Paragraph::all()->where('name', $paragraph->name)->where('capital_id', '<=', $request->capital_id)->where('capital_id', '>' , $paragraph->capital_id);
                $c = 0;
                foreach($counts as $count){
                    $count->capital_id = $paragraph->capital_id + $c;
                    $count->save();
                    $c +=1;
                }
                $paragraph->capital_id = $request->capital_id;
                $paragraph->save();
            }
            elseif($request->draft == true && $paragraph->draft == false){
            $count=Paragraph::all()->where('name', $paragraph->name)->where('capital_id','!=' , 0 )->count();
            $sum= $count +1;
            $paragraph->capital_id = $sum;
            $paragraph->draft=true;
            $paragraph->save();
            }
            elseif($request->draft == false){
                $paragraph->capital_id = 0;
                $paragraph->draft=false;
                $paragraph->save();
               
                $counts=Paragraph::all()->where('name', $paragraph->name)->where('capital_id','!=' , 0 )->sortBy('capital_id');
                $c = 0;
           
                foreach($counts as $count){
                    $count->capital_id = $c +1;
                    $count->save();
                    $c +=1;
                }
                }
            else{
            }
            $paragraph->name = $paragraph->name;
            $paragraph->body = $request->body;
            $paragraph->title = $request->title;
            $paragraph->save();

         return redirect(route('article.specificshowparagraph',compact('paragraph')));
     }

     public function destroy(Paragraph $paragraph){
         $article = Article::all()->where('title', $paragraph->name)->first()->id; 
         $paragraph->delete();
         return redirect(route('article.specificlist', compact('article')));
     }

     public function change(Paragraph $paragraph , Request $request){

        if($paragraph->capital_id > $request->capital_id && $paragraph->capital_id !== 0){
            $paragraph->capital_id = 0;
            $paragraph->save();
            $counts=Paragraph::all()->where('name', $paragraph->name)->where('capital_id', '>=', $request->capital_id);
            $c = 0;
            foreach($counts as $count){
                $count->capital_id = $request->capital_id + 1 + $c;
                $count->save();
                $c +=1;
                $paragraph->capital_id = $request->capital_id;
                $paragraph->save();
            }
        } 
        elseif($paragraph->capital_id < $request->capital_id && $paragraph->capital_id !== 0){
            $counts=Paragraph::all()->where('name', $paragraph->name)->where('capital_id', '<=', $request->capital_id)->where('capital_id', '>' , $paragraph->capital_id);
            $c = 0;
            foreach($counts as $count){
                $count->capital_id = $paragraph->capital_id + $c;
                $count->save();
                $c +=1;
            }
            $paragraph->capital_id = $request->capital_id;
            $paragraph->save();
        }
    
        return redirect()->back();

     }
     public function draft(Paragraph $paragraph){
        $paragraph->capital_id = 0;
        $paragraph->draft=false;
        $paragraph->save();
       
        $counts=Paragraph::all()->where('name', $paragraph->name)->where('capital_id','!=' , 0 )->sortBy('capital_id');
        $c = 0;
   
        foreach($counts as $count){
            $count->capital_id = $c +1;
            $count->save();
            $c +=1;
        }
        
        $article = Article::all()->where('title', $paragraph->name)->first()->id; 
        return redirect(route('article.specificlist', compact('article')));
     }
}
