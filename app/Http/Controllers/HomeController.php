<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Draft;
use App\Models\Paragraph;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
 /*    public function __construct()
    {
        $this->middleware('auth');
    } */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function show(Article $article){
    
        if($article->draft === 1){
            $articlerev = $article;
        } 
        elseif($article->draft === 0 && Auth::user()){
            $articlerev = $article;
        }
        else{
            return redirect('/');
        }

        $paragraphs=Paragraph::all()->where('name', $article->title)->sortBy('capital_id')->where('draft' , true);
       

        return view('article.show', compact('articlerev','paragraphs' ));
    }
}
 