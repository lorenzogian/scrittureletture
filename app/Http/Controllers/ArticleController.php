<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticleRequest;
use App\Http\Requests\EditArticleRequest;
use App\Models\Article;
use App\Models\NameParagaph;
use App\Models\Paragraph;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('article.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        /* dd( $request->img); */
        $paragraph = new  NameParagaph();
        $paragraph->name = $request->title;
        $paragraph->save();

        if($request->draft == true){
            $article = new Article();
            $article->title = $request->title;
            $article->body = $request->body;
            $article->draft = $request->draft;
            $article->synopsis = $request->synopsis;
            $article->paragraph = $request->title;
            $article->img = $request->img == null ? 2 : $request->img->store('public/img');
            $article->save();
            
          }else{
            $article = new Article();
            $article->title = $request->title;
            $article->body = $request->body;
            $article->draft = false;
            $article->synopsis = $request->synopsis;
            $article->paragraph = $request->title;
            $article->img = $request->img == null ? 2 : $request->img->store('public/img');
            $article->save();
          }
    
          return redirect('/lista-articoli');

    }

        public function list()
        {
            return view('list');
        }

 

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
  

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $articlerev)
    {
        return view('article.edit', compact('articlerev'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Article $articlerev, EditArticleRequest $request)
    {
      
        
        if($request->draft == true){
            $nameparpraphs = NameParagaph::all()->where('name', $articlerev->title);
            foreach($nameparpraphs as $nameparpraph){
                $nameparpraph->name = $request->title;
                $nameparpraph->save();
            }
            $parpraphs = Paragraph::all()->where('name', $articlerev->title);
            foreach(  $parpraphs as $parpraph){
                $parpraph->name = $request->title;
                $parpraph->save();
            }
            $articlerev->title = $request->title;
            $articlerev->body = $request->body;
            $articlerev->draft = $request->draft;
            $articlerev->synopsis = $request->synopsis;
            $articlerev->paragraph = $request->title;
            $articlerev->save();
            return redirect('/');
            
          }else{
            $nameparpraphs = NameParagaph::all()->where('name', $articlerev->title);
            foreach($nameparpraphs as $nameparpraph){
                $nameparpraph->name = $request->title;
                $nameparpraph->save();
            }
            $parpraphs = Paragraph::all()->where('name', $articlerev->title);
            foreach(  $parpraphs as $parpraph){
                $parpraph->name = $request->title;
                $parpraph->save();
            }
            $articlerev->title = $request->title;
            $articlerev->body = $request->body;
            $articlerev->draft = false;
            $articlerev->synopsis = $request->synopsis;
            $articlerev->paragraph = $request->title;
            $articlerev->save();        
            return redirect()->back();
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $paragraphsname=NameParagaph::all()->where('name' , $article->paragraph);
        foreach($paragraphsname as $paragraphname){
            $paragraphname->delete();
        } 
       
        $paragraphs=Paragraph::all()->where('name' , $article->paragraph);
        foreach($paragraphs as $paragraph){
            $paragraph->delete();
        } 

        $article->delete();
        return redirect(route('list'));
    }

    public function special(Article $article){
        $articlerev=$article;
        $paragraphs = Paragraph::all()->where('name', $article->title)->sortBy('capital_id');
        $counts = Paragraph::all()->where('name', $article->title)->where('capital_id','!=' , 0 )->sortBy('capital_id');

        return view('article.specificlist', compact('articlerev' , 'paragraphs', 'article', 'counts'));
    }

    public function specificshow(Article $article){
       return view('article.specificshow', compact('article'));
    }

    public function finished(Article $articlerev){
        if($articlerev->progress == 0){
            $articlerev->progress = true;
            $articlerev->save();
        }
        else{
            $articlerev->progress = false;
            $articlerev->save();
        }
        return redirect('/');
    }
}
