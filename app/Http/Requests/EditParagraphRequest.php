<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditParagraphRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return boolz
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required',
            'body'=>'required',
           /*  'synopsis'=>'required|string|max:200', */
       /*      'img' => 'required|mimes:jpeg,png|max:1024' */
         
        ];
    }
}
