<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|string|max:120|unique:articles,title',
            'synopsis'=>'required|string|max:200',
            'img' => 'required|mimes:jpeg,png|max:1024|dimensions:min_width=420,min_height=200'
         
        ];
    }
}
