<?php

namespace App\Providers;

use App\Models\Article;
use Illuminate\Support\Facades\View;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
        public function boot()
        {
             if (Schema::hasTable('articles')){
                $articles = Article::all()->where('draft', true)->where('progress', true);
                View::share('articles', $articles);
            }

            if (Schema::hasTable('articles')){
                $articlesprogress = Article::all()->where('draft', true)->where('progress', false);
                View::share('articlesprogress', $articlesprogress);
            }

            if (Schema::hasTable('articles')){
                $a= Article::all();
                View::share('a', $a);
               
            }
     }
}


/*  Paginator::useBootstrap(); */

