<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NameParagaph extends Model
{
    use HasFactory;
    protected $fillabole = ['name'];
}
